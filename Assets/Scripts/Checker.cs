﻿using UnityEngine;
using UnityEditor;

public class Checker : MonoBehaviour
{
    private int goundLayerMask = 1 << 11; //ignore all layers but the ground layer

    //check what face of dice is up
    public bool Up()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 0.1f, goundLayerMask);

        return hitColliders.Length > 0;
    }
}
