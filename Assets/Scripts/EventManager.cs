﻿using System;

public class EventManager : Singleton<EventManager>
{
    public event Action<bool> onRoll;
    public void Roll(bool rolling)
    {
        if (onRoll != null)
            onRoll(rolling);
    }

    public event Action<int> onResult;
    public void Result(int result)
    {
        if (onResult != null)
            onResult(result);
    }

    public event Action onRollPressed;
    public void RollPressed()
    {
        if (onRollPressed != null)
            onRollPressed();
    }
}
