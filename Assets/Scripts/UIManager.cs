﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI resultText;

    [SerializeField]
    private TextMeshProUGUI totalText;

    [SerializeField]
    private Button rollBtn;

    private int total = 0;

    private void Start()
    {
        EventManager.Instance.onResult += SetResult;
        EventManager.Instance.onResult += AddTotal;

        rollBtn.onClick.AddListener(RollPressed);
    }

    private void SetResult(int result)
    {
        if(result == 0)
            resultText.text = "Result: ?";
        else
            resultText.text = "Result: " + result.ToString();
    }

    private void AddTotal(int result)
    {
        total += result;
        totalText.text = "Total: " + total.ToString();
    }

    private void RollPressed()
    {
        EventManager.Instance.RollPressed();
    }
    
}
