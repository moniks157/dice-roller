﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private int interactibleLayerMask = 1 << 9; //ignore all layers but the interactible layer
    private int mouseDetectionLayerMask = 1 << 10; //ignore all layers but the mouse detection layer

    private bool isMouseDragging = false;
    private Dice dice = null;

    [SerializeField]
    private float velocityMagnitude = 10f;
    private Vector3 velocity;

    private bool enableInput = false;

    private void Start()
    { 
        EventManager.Instance.onRoll += EnableInput;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && enableInput)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            //check if dice is clicked
            if (Physics.Raycast(ray, out hit, 100.0f, interactibleLayerMask))
            {
                isMouseDragging = true;
                dice = hit.collider.gameObject.GetComponent<Dice>();
                velocity = dice.transform.position;
            }
        }

        if(Input.GetMouseButtonUp(0) && dice != null)
        {
            isMouseDragging = false;
            dice.Roll();
            dice = null;
        }

        if(isMouseDragging)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(ray, out hit, 100.0f, mouseDetectionLayerMask))
            {
                velocity = hit.point - dice.transform.position;
            }

            //drag dice using mouse
            dice.Move(velocity * velocityMagnitude);
        }
    }

    // allows to enable/disable input 
    private void EnableInput(bool disable)
    {
        enableInput = !disable;
    }
}
