﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour
{

    [Serializable]
    public class DiceSide
    {
        public GameObject diceSide;
        public int number;
    }

    [SerializeField]
    private float minRandomSpeed = 5f;

    [SerializeField]
    private List<DiceSide> diceSides;

    private Rigidbody rb;
    private Vector3 origin;

    private bool rolling = false;
    private System.Random r;

    private void Start()
    {
        origin = transform.position;
        rb = GetComponent<Rigidbody>();
        r = new System.Random();

        EventManager.Instance.onRollPressed += RollPressed;

        SetDiceSides();
        StartCoroutine(SetUp());
    }

    private void Update()
    {
        if (rolling)
        {
            if (rb.IsSleeping())
            {
                var result = 0;
                foreach (DiceSide ds in diceSides)
                {
                    if (ds.diceSide != null)
                    {
                        //which side of dice is up
                        if (ds.diceSide.GetComponentInChildren<Checker>().Up())
                            result = ds.number;
                    }
                }

                //add velocity in case of undetermined result (dice stopping on edge)
                if (result == 0)
                {
                    rb.velocity = new Vector3((float)r.NextDouble(), (float)r.NextDouble(), (float)r.NextDouble());
                }

                if(result > 0)
                {
                    rolling = false;

                    rb.constraints = RigidbodyConstraints.FreezeRotation;

                    EventManager.Instance.Roll(rolling);
                    EventManager.Instance.Result(result);
                }
                
            }
        }
    }

    public void Move(Vector3 velocity)
    {
        rb.velocity = velocity;
    }

    public void Roll()
    {
        //check if velocity is high enough to result in "random" number
        if (rb.velocity.magnitude > minRandomSpeed)
        { 
            rolling = true;

            rb.constraints = RigidbodyConstraints.None;

            EventManager.Instance.Roll(rolling);
            EventManager.Instance.Result(0);
        }
    }

    public void RollPressed()
    {
        if (rb.IsSleeping())
        {
            //reset dice positionto orgin
            transform.position = origin;

            //give dice random speed
            rb.velocity = Vector3.forward * ((float)r.NextDouble() + 10f);

            Roll();
        }
    }

    private void SetDiceSides()
    {
        if (diceSides != null)
        {
            foreach (DiceSide ds in diceSides)
            {
                if (ds.diceSide == null)
                {
                    Debug.LogError("Missing a dice side");
                    return;
                }

                var point = "";

                if (ds.number == 6 || ds.number == 9)
                {
                    point = ".";
                }

                ds.diceSide.GetComponent<TextMesh>().text = ds.number.ToString() + point;
            }
        }
    }

    private IEnumerator SetUp()
    {
        rb.AddTorque(transform.forward * 5f);

        yield return new WaitUntil(() => rb.IsSleeping() == true);

        rb.constraints = RigidbodyConstraints.FreezeRotation;

        EventManager.Instance.Roll(false);
    }

}
